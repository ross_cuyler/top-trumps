import { AuthService } from '../../services/auth/auth.service';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType, createEffect } from '@ngrx/effects';

import { concatMap, mergeMap, catchError } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import * as actions from './app-state.actions';
import { ActionService } from '../../services/action/action.service';

@Injectable()
export class AppStateEffects {
    // #region ReqLinkUser
    ReqLinkUser$ = createEffect(() =>
        this.actions$.pipe(
            ofType(actions.ReqLinkUser),
            mergeMap(payload => {
                return of(payload).pipe(
                    mergeMap(_payload => {
                        return this.authService.getCurrentUser();
                    }),
                    mergeMap(user => {
                        return [
                            actions.LinkUserSuccess({ executedTime: payload.executedTime }),
                            this.actionService.authActions.UpdateUserFromInit({
                                user,
                                executedTime: payload.executedTime,
                            }),
                        ];
                    }),
                    catchError(error => {
                        console.log('ReqLinkUser: ', error.message);
                        return [actions.LinkUserFail({ executedTime: payload.executedTime })];
                    }),
                );
            }),
        ),
    );
    // #endregion
    constructor(private actions$: Actions, private authService: AuthService, private actionService: ActionService) {}
}
