import { createReducer, Action, on } from '@ngrx/store';
import { Dictionary } from '@ngrx/entity';
import * as actions from './app-state.actions';
import { ClearState } from '../authState/auth.actions';
import { addLoading, removeLoading } from '../helperFunctions';

export const appStateFeatureKey = 'appState';

export interface State {
    initialLogin: boolean;
    isLoading: boolean;
    persist?: boolean;
    dictLoading: Dictionary<string>;
}

export const initialState: State = {
    initialLogin: false,
    persist: true,
    isLoading: false,
    dictLoading: {},
};

const appStateReducer = createReducer(
    initialState,
    on(actions.ReqToggleIsLoading, (state, { ...payload }) => {
        return { ...state, isLoading: payload.flag };
    }),
    on(actions.ReqLinkUser, (state, { ...payload }) => {
        return addLoading({ ...state, persist: true }, payload);
    }),
    on(actions.LinkUserSuccess, actions.LinkUserFail, (state, { ...payload }) => {
        return removeLoading({ ...state, persist: false }, payload);
    }),
    on(ClearState, () => {
        return { ...initialState, persist: false };
    }),
);

export function reducer(state = initialState, action: Action): State {
    return appStateReducer(state, action);
}
