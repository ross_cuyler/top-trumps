import { Action, createAction, props } from '@ngrx/store';

export const ReqToggleIsLoading = createAction('[App] ReqToggleIsLoading', props<{ flag: boolean }>());

// #region Link the user state to firebase
export const ReqLinkUser = createAction('[App] ReqLinkUser', props<{ executedTime: number }>());
export const LinkUserFail = createAction('[ReqLinkUser] LinkUserFail', props<{ executedTime: number }>());
export const LinkUserSuccess = createAction('[ReqLinkUser] [SCU] LinkUserSuccess', props<{ executedTime: number }>());
//#endregion
