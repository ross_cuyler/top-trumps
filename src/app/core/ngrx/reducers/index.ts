import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import * as fromAppState from '../app-state/app-state.reducer';
import * as fromAuth from '../authState/auth.reducer';

export interface State {
    appState: fromAppState.State;
    auth: fromAuth.State;
}

export const reducers: ActionReducerMap<State> = {
    appState: fromAppState.reducer,

    auth: fromAuth.reducer,
};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

// #region Selectors

// #region Feature Selectors
export const getAppState = createFeatureSelector<State, fromAppState.State>('appState');
export const getAuthState = createFeatureSelector<State, fromAuth.State>(fromAuth.authFeatureKey);
// #endregion

//#endregion

// #region App State
// export const getAppIsLoading
//#endregion
