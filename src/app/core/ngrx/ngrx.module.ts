import { MatSnackBarModule } from '@angular/material/snack-bar';
import { State } from './reducers/index';
import { environment } from 'src/environments/environment';
import { AppStateEffects } from './app-state/app-state.effects';
import { AuthEffects } from './authState/auth.effects';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { ReqLinkUser } from './app-state/app-state.actions';

@NgModule({
    imports: [
        MatSnackBarModule,
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
            },
        }),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        EffectsModule.forRoot([AuthEffects, AppStateEffects]),
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: (store: Store<State>) => {
                return () => {
                    // store.dispatch(ReqToggleIsLoading({ flag: true }));
                    store.dispatch(ReqLinkUser({ executedTime: Date.now() }));
                };
            },
            multi: true,
            deps: [Store],
        },
    ],
    exports: [StoreModule, StoreDevtoolsModule, EffectsModule],
})
export class NGRXModule {}
