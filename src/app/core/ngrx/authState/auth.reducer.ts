import { Dictionary } from '@ngrx/entity';
import * as actions from './auth.actions';
import { Action, createReducer, on } from '@ngrx/store';
import { getStateData, addLoading, removeLoading, saveStateData } from '../helperFunctions';
import { User } from '../../models/baseModels';

export const authFeatureKey = 'auth';

export interface State {
    user?: User;
    dictLoading: Dictionary<string>;
}

const initialState: State = getInitialState();

function getInitialState() {
    const initState = getStateData<State>(authFeatureKey);

    return initState;
}

const authReducer = createReducer(
    initialState,
    on(actions.ReqEmailLogin, actions.ReqEmailRegister, actions.ReqProviderSignIn, (state, { ...payload }) => {
        return addLoading<State>(state, payload);
    }),
    on(
        actions.UpdateUserFromInit,
        actions.EmailLoginSuccess,
        actions.EmailRegisterSuccess,
        actions.ProviderSignInSuccess,
        (state, { ...payload }) => {
            let newState = removeLoading(state, payload);
            newState = { ...newState, user: payload.user };
            saveStateData(authFeatureKey, newState, ['user']);
            return newState;
        },
    ),

    on(actions.EmailRegistrationFail, actions.ProviderSignInFail, actions.EmailLoginFail, (state, { ...payload }) => {
        return removeLoading(state, payload);
    }),
    on(actions.ReqLogoutAction, () => {
        return null;
    }),
);

export function reducer(state = initialState, action: Action): State {
    return authReducer(state, action);
}
