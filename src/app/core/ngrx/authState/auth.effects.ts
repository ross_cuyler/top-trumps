import { SubscriptionService } from '../../services/subscription/subscription.service';
import { ActionService } from '../../services/action/action.service';
import { Injectable } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Actions, ofType, createEffect } from '@ngrx/effects';

import { combineLatest, of } from 'rxjs';
import * as actions from './auth.actions';
import { Store, select, Action } from '@ngrx/store';
import * as fromRoot from '../reducers/index';
import { mergeMap, take, catchError, tap, switchMap, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthEffects {
    // #region ReqEmailLogin
    ReqEmailLogin$ = createEffect(() =>
        this.actions$.pipe(
            ofType(actions.ReqEmailLogin, actions.ReqProviderSignIn),

            mergeMap(payload => {
                return combineLatest([of(payload), this.store.pipe(select(fromRoot.getAppState), take(1))]).pipe(
                    takeUntil(this.subService.unsubscribe$),
                    mergeMap(async ([_payload, appState]) => {
                        this.store.dispatch(this.actionService.appStateActions.ReqToggleIsLoading({ flag: true }));
                        let result: 'fail' | { route: string };

                        switch (payload.type) {
                            case '[Login Page] ReqEmailLogin':
                                result = await this.authService.emailLogin(payload.email, payload.password);
                                break;
                            case '[Register Page] ReqProviderSignIn':
                            default:
                                result = await this.authService.providerLogin(payload.provider);
                                break;
                        }
                        if (typeof result === 'string') {
                            throw new Error(result);
                        }

                        // TODO: FIXME: Custom Routing for Each app happens here.
                        if (appState.initialLogin) {
                            this.routeToAccount(result.route);
                        }

                        return this.authService.getCurrentUser();
                    }),
                    mergeMap(user => user),

                    mergeMap((user, i) => {
                        if (i === 0) {
                            this.router.navigate([environment.DASHBOARD_ROUTE]);
                            // TODO: FIXME: Include { type: '[ReqEmailLogin] Other Login Actions' },
                            return [
                                actions.EmailLoginSuccess({ user, executedTime: payload.executedTime }),
                                this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                            ];
                        }
                        return [{ type: 'actions.EmailLoginSuccess({ user, executedTime: payload.executedTime })' }];
                    }),

                    catchError(error => {
                        console.log('ReqEmailLogin: ', error.message);
                        const arActions: Action[] = [
                            this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                        ];

                        switch (payload.type) {
                            case '[Login Page] ReqEmailLogin':
                                arActions.push(actions.EmailLoginFail({ executedTime: payload.executedTime }));
                                break;
                            case '[Register Page] ReqProviderSignIn':
                            default:
                                arActions.push(actions.ProviderSignInFail({ executedTime: payload.executedTime }));
                                break;
                        }
                        this.snackbar.open(this.constructErrorMessage(error.message), 'Login', { duration: 4000 });
                        return arActions;
                    }),
                );
            }),
        ),
    );
    // #endregion

    // #region Registration
    ReqEmailRegister$ = createEffect(() =>
        this.actions$.pipe(
            ofType(actions.ReqEmailRegister),
            mergeMap(payload => {
                return combineLatest([of(payload), this.store.pipe(select(fromRoot.getAppState), take(1))]).pipe(
                    mergeMap(async ([_payload, appState]) => {
                        this.store.dispatch(this.actionService.appStateActions.ReqToggleIsLoading({ flag: true }));
                        const result = await this.authService.emailSignUp(
                            _payload.email,
                            _payload.password,
                            _payload.signUpData,
                        );
                        if (typeof result === 'string') {
                            throw new Error(result);
                        }

                        // TODO: FIXME: Custom Routing for Each app happens here.
                        if (appState.initialLogin) {
                            this.routeToAccount(result.route);
                        }

                        return this.authService.getCurrentUser();
                    }),
                    mergeMap(user => user),
                    mergeMap((user, i) => {
                        if (i === 0) {
                            // TODO: FIXME: Include { type: '[ReqEmailLogin] Other Login Actions' },
                            this.router.navigate([environment.DASHBOARD_ROUTE]);
                            return [
                                actions.EmailRegisterSuccess({ user, executedTime: payload.executedTime }),
                                this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                            ];
                        }
                        return [actions.EmailRegisterSuccess({ user, executedTime: payload.executedTime })];
                    }),
                    catchError(error => {
                        console.log('ReqEmailRegister: ', error.message);
                        this.snackbar.open(this.constructErrorMessage(error.message), 'Login', { duration: 4000 });
                        return [
                            actions.EmailRegistrationFail({ executedTime: payload.executedTime }),
                            this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                        ];
                    }),
                );
            }),
        ),
    );
    // #endregion

    // #region ReqLogoutAction
    ReqLogoutAction$ = createEffect(() =>
        this.actions$.pipe(
            ofType(actions.ReqLogoutAction),
            mergeMap(payload => {
                return of(payload).pipe(
                    mergeMap(_payload => {
                        return this.authService.logout();
                    }),
                    mergeMap(() => {
                        this.subService.unsubscribeComponent$.next();
                        localStorage.clear();
                        return of('disposable').pipe(
                            switchMap(() => {
                                return [actions.ClearState()];
                            }),
                        );
                    }),
                    catchError(error => {
                        console.log('ReqLogoutAction: ', error.message);
                        return [];
                    }),
                );
            }),
        ),
    );
    // #endregion

    // #region ReqProviderSignIn
    ReqProviderSignIn$ = createEffect(() =>
        this.actions$.pipe(
            ofType(actions.ReqProviderSignIn),
            mergeMap(payload => {
                return of(payload).pipe(
                    mergeMap(async _payload => {
                        const result = await this.authService.providerLogin(_payload.provider);
                        console.log(result);
                        if (typeof result === 'string') {
                            throw new Error(this.constructErrorMessage(result));
                        }
                        return [
                            actions.ProviderSignInSuccess({ user: result, executedTime: _payload.executedTime }),
                            this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                        ];
                    }),
                    mergeMap(arActions => arActions),
                    catchError(error => {
                        console.log('ReqProviderSignIn: ', error.message);
                        return [
                            actions.ProviderSignInFail({ executedTime: payload.executedTime }),
                            this.actionService.appStateActions.ReqToggleIsLoading({ flag: false }),
                        ];
                    }),
                );
            }),
        ),
    );
    // #endregion

    // #region Helper functions
    routeToAccount(route: string) {
        this.router.navigate([route]);
    }

    constructErrorMessage(code: string) {
        let message = '';

        switch (code) {
            case 'auth/email-already-exists':
            case 'auth/email-already-in-use':
                message = 'The provided email is already in use. Please sign in or reset your password.';

                break;
            case 'auth/wrong-password':
                message = 'The email or password is incorrect. Please try again.';

                break;
            case 'auth/popup-closed-by-user':
                message = 'Login has been cancelled by the user.';
                break;
            case 'auth/popup-blocked':
                message = 'The login popup has been blocked. Please allow popups to continue.';
                break;
            case 'auth/cancelled-popup-request':
                message = 'The login request has been cancelled.';
                break;
            case 'auth/timeout':
                message = 'The action took too long. Please check your network connection and try again.';
                break;
            case 'auth/account-exists-with-different-credential':
                message = 'This account already exists in our system.';
                break;
            case 'auth/too-many-requests':
                message = 'Too many incorrect attempts have been made. Please try again in 1 minute.';
                break;
            case 'auth/invalid-email':
                message = 'Please provide a full valid email address.';
                break;
            case 'auth/user-not-found':
                message = 'There is no user with the provided credentials.';
                break;
            case 'auth/user-disabled':
                message = 'This user account has been disabled by the developers.';
                break;
            case 'auth/signed-in':
                message = 'Please log out to register a new account.';
                break;
            case 'auth/operation-not-allowed':
                message = 'This feature has not been enabled yet. Please use one of the other methods to login.';
                break;
            default:
                console.log({ code });
                if (code.length < 15) {
                    message = 'We ran into an authentication error.\nPlease try again.';
                } else {
                    message = 'An unexpected error has occurred and your action was cancelled. Please try again. ';
                }
                break;
        }

        return message;
    }
    // #endregion

    constructor(
        private router: Router,
        private actions$: Actions,
        private snackbar: MatSnackBar,
        private authService: AuthService,
        private actionService: ActionService,
        private store: Store<fromRoot.State>,
        private subService: SubscriptionService,
    ) {}
}
