

export interface User {
    name: string;
    surname?: string;
    email: string;
    uid: string;
}

export interface Deck {

    uid: string;
    name: string;
    imageurl: string;

}

export interface Card {
    attributes: Attribute[];
    name: string;
    imageurl: string;

}

export interface Attribute {

    type: string;
    value: number;
    highWin: boolean;

}

export interface DeckofCards extends Deck {

    cards: Card[];
}


