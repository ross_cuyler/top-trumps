import { Injectable } from '@angular/core';
import * as authActions from '../../ngrx/authState/auth.actions';
import * as appStateActions from '../../ngrx/app-state/app-state.actions';

@Injectable({
    providedIn: 'root',
})
export class ActionService {
    constructor() {}

    get authActions() {
        return authActions;
    }

    get appStateActions() {
        return appStateActions;
    }
}
