import { TestBed } from '@angular/core/testing';

import { UploadDetailsService } from './upload-details.service';

describe('UploadDetailsService', () => {
  let service: UploadDetailsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploadDetailsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
