import { SubscriptionService } from './../subscription/subscription.service';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { switchMap, take, tap, share, mergeMap, takeUntil } from 'rxjs/operators';
import { of, Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireFunctions } from '@angular/fire/functions';
import { User } from '../../models/baseModels';
import { SignUpData } from '../../models/componentModels';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    user$: Observable<User>;

    constructor(
        private router: Router,
        private afs: AngularFirestore,
        private afAuth: AngularFireAuth,
        private fns: AngularFireFunctions,
        private subService: SubscriptionService,
    ) 
    
    {
        this.user$ = afAuth.authState.pipe(
            mergeMap((firebaseUser) => {
                if (firebaseUser) {
                    return this.afs.doc<User>(`Users/${firebaseUser.uid}`).valueChanges();
                } else {
                    return of(null);
                }
            }),
            share(),
        );
    }

    getCurrentUser() {
        return this.user$.pipe();
    }

    async emailLogin(email: string, password: string) {
        try {
            return await this.afAuth.auth.signInWithEmailAndPassword(email, password);
        } catch (error) {
            return error.code;
        }
    }

    async providerLogin(provider) {
        try {
            const firebaseUser = await this.afAuth.auth.signInWithPopup(provider);

            const obUser: User = {
                name: firebaseUser.user.displayName,
                email: firebaseUser.user.email,
                uid: firebaseUser.user.uid,
            };

            await this.updateUserData(obUser);
            return obUser;
        } catch (error) {
            console.log('providerLogin', { error });
            return error.code ? error.code : error.message;
        }
    }

    async emailSignUp(email: string, password: string, signUpData: SignUpData) {
        try {
            // const callable = this.fns.httpsCallable('registerUser')
            const firebaseUser = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);

            // Build the user for the current app depending on the base model
            const obUser: User = {
                email,
                name: signUpData.name,
                surname: signUpData.surname,
                uid: '',
            };

            const userRef = this.afs.doc(`Users/${firebaseUser.user.uid}`);
        
            obUser.uid = firebaseUser.user.uid
            await userRef.set(obUser); // Creates the document for the first time user on the first login.
          
          

            // await callable({ obUser, password }).toPromise();
            // const firebaseUser = await this.afAuth.auth.signInWithEmailAndPassword(email, password);

            return { firebaseUser, obUser };
        } catch (error) {
            console.log(error);
            return error.message;
        }
    }

    async updateUserData(data: User) {
        try {
            const userRef = this.afs.doc(`Users/${data.uid}`);
            const doesExist: boolean = await this.checkIfRefExists(userRef);
            if (!doesExist) {
                userRef.set(data); // Creates the document for the first time user on the first login.
            } else {
                userRef.update(data); // Updates the user if the action is requested.
            }
            return 'success';
        } catch (error) {
            console.log('user Update Error', error);
            return Promise.reject('fail');
        }
    }

    async checkIfRefExists(ref: AngularFirestoreDocument) {
        const snap = await ref.get().pipe(take(1)).toPromise();
        return snap.exists ? true : false;
    }

    resetPassword(email: string) {
        this.afAuth.auth.sendPasswordResetEmail(email);
    }

    async logout() {
        this.router.navigate(['login']);
        await this.afAuth.auth.signOut();
    }
}
