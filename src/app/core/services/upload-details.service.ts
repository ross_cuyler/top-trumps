import { take, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { DeckofCards, Deck, Card } from '../models/baseModels';
import { ThrowStmt } from '@angular/compiler';
import * as firebase from 'firebase';

@Injectable({
    providedIn: 'root',
})
export class UploadDetailsService {

  cards;
  decks;
  newDeck;
  arFilteredDecks: DeckofCards[] = [];

    constructor(private afs: AngularFirestore) {}

    CreateDeckData(name: string, imageurl: string) {
        const deckdata: Deck = {
            uid: this.afs.createId(),
            name: name,
            imageurl: imageurl,
        };
        this.afs.collection('Decks').doc(deckdata.uid).set(deckdata);
    }

    UpdateDeckData(name: string, imageurl: string, uid: string) {
        const deckdata: Deck = {
            uid: uid,
            name: name,
            imageurl: imageurl,
        };
        this.afs.collection('Decks').doc(deckdata.uid).update(deckdata);
    }

    async ReadDeck() {
        // this.afs.collection("Decks").get().subscribe(decks=> {
        //   decks.forEach(deck=>{.
        //     console.log(deck.data())
        //   })
        // })
        
        this.decks = await this.afs
            .collection('Decks')
            .get().pipe(take(1),map(snap => snap.docs.map(doc => doc.data())))
            .toPromise() ;
        return this.decks;
    }

    async ReadCard(deckid: string) {
      // this.afs.collection("Decks").get().subscribe(decks=> {
      //   decks.forEach(deck=>{.
      //     console.log(deck.data())
      //   })
      // })
      
      this.cards = await this.afs
          .collection("Decks").doc(deckid).collection("Cards").doc("cards")
          .get().pipe(take(1),map(snap => snap.data().cards))
          .toPromise() ;
      return this.cards;
  }

    getDecks(){
      return this.arFilteredDecks;
    }

    async UploadCards(card: Card, deckid: string)
    {
        this.afs.collection("Decks").doc(deckid).collection("Cards").doc("cards").update({cards: firebase.firestore.FieldValue.arrayUnion(card)})
    }
}
