import { ActionService } from '../../core/services/action/action.service';
import { AuthService } from '../../core/services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../core/ngrx/reducers/index';
import { map, take, tap } from 'rxjs/operators';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
    user$;
    constructor(
        public authService: AuthService,
        public store: Store<fromRoot.State>,
        private actionService: ActionService,
    ) {}

    handleLogout() {
        this.store.dispatch(this.actionService.authActions.ReqLogoutAction());
    }

    ngOnInit() {
        this.user$ = this.store.pipe(
            select(fromRoot.getAuthState),
            map(x => x.user),
            take(1),
        );
    }
}


