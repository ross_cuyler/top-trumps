import { Component, OnInit } from '@angular/core';
import { UploadDetailsService } from 'src/app/core/services/upload-details.service';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  cards = [];

  constructor( private decksService: UploadDetailsService) {
/*     this.decks = decksService.ReadDeck();
    console.log(this.decks); */
    
   }
  

  async ngOnInit() {

     this.cards = await this.decksService.ReadCard("HG7TyK53xF86b945xmcA");
    
    console.log(this.cards);
    
  }

}
