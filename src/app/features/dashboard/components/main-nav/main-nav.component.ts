import { AuthService } from 'src/app/core/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
    selector: 'app-main-nav',
    templateUrl: './main-nav.component.html',
    styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent implements OnInit {
    LOGO = environment.LOGO;

    obNavState: { isMobile: boolean } = { isMobile: false };

    arSubs: Subscription[] = [];

    constructor(private breakpointObserver: BreakpointObserver, public authService: AuthService) {}

    ngOnInit() {
        this.arSubs.push(
            this.breakpointObserver.observe(['(min-width: 764px)']).subscribe((state: BreakpointState) => {
                if (state.matches) {
                    this.obNavState.isMobile = false;
                } else {
                    this.obNavState.isMobile = true;
                }
            }),
        );
    }

    handleLogout() {
        
    }
}
