import { RuleComponent } from './../../rule/rule.component';

import { HomeComponent } from './../../home/home.component';
import { AngularMaterialModule } from './../shared/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { ChooseGameComponent } from './components/choose-game/choose-game.component';
import { CardComponent } from './components/card/card.component';

@NgModule({
    declarations: [DashboardComponent, CardComponent, MainNavComponent, HomeComponent, ChooseGameComponent, RuleComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, AngularMaterialModule, DashboardRoutingModule],
})
export class DashboardModule {}
