import { ActionService } from 'src/app/core/services/action/action.service';
import { environment } from './../../../environments/environment';
import { SignUpData } from '../../core/models/componentModels';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import * as firebase from 'firebase';
import * as fromRoot from '../../core/ngrx/reducers/index';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    signUpForm: FormGroup;

    // #region project specific variables
    LOGO = environment.LOGO; // Change out for different Apps
    // #endregion

    // #region Getters for the form fields
    // #region SignUpForm Getters
    get emailRegister() {
        return this.signUpForm.get('emailRegister');
    }
    get passwordRegister() {
        return this.signUpForm.get('passwordRegister');
    }
    get confirmPassword() {
        return this.signUpForm.get('confirmPassword');
    }
    get name() {
        return this.signUpForm.get('name');
    }
    get surname() {
        return this.signUpForm.get('surname');
    }
    // #endregion

    //  #region LoginForm Getters
    get email() {
        return this.loginForm.get('email');
    }
    get password() {
        return this.loginForm.get('password');
    }
    // #endregion
    // #endregion

    constructor(
        private fb: FormBuilder,
        private actionService: ActionService,
        private store: Store<fromRoot.State>,
    ) {}

    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', [Validators.required]],
        });
        this.signUpForm = this.fb.group(
            {
                name: ['', [Validators.required, Validators.minLength(2)]],
                surname: ['', [Validators.required, Validators.minLength(2)]],
                emailRegister: ['', [Validators.email, Validators.required]],
                passwordRegister: [
                    '',
                    [
                        Validators.required,
                        Validators.pattern('^(.*)(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)(.*)$'),
                        Validators.minLength(6),
                    ],
                ],
                confirmPassword: ['', [Validators.required]],
            },
            { validator: this.checkPassword },
        );
    }

    async handleLogin() {
        const email = this.email.value as string;
        const password = this.password.value as string;
        this.store.dispatch(
            this.actionService.authActions.ReqEmailLogin({ email, password, executedTime: Date.now() }),
        );
    }

    handleGoogleLogin() {
        const provider = new firebase.auth.GoogleAuthProvider();
        // FIXME: The service function handling this is not working as intended.
        this.store.dispatch(this.actionService.authActions.ReqProviderSignIn({ provider, executedTime: Date.now() }));
    }

    // handleFacebookLogin() {}

    handleRegister() {
        const email = this.emailRegister.value as string;
        const password = this.passwordRegister.value as string;
        const confirmPassword = this.confirmPassword.value as string;
        const name = this.name.value as string;
        const surname = this.surname.value as string;

        const data: SignUpData = { name, surname };

        this.store.dispatch(
            this.actionService.authActions.ReqEmailRegister({
                email,
                password,
                signUpData: data,
                executedTime: Date.now(),
            }),
        );
    }
    handleLogout() {
        this.store.dispatch(this.actionService.authActions.ReqLogoutAction());
    }

    checkPassword(g: FormControl) {
        const password = g.get('passwordRegister');
        const confirmPassword = g.get('confirmPassword');

        return password.value === confirmPassword.value ? null : confirmPassword.setErrors({ notEquivalent: true });
    }
}
