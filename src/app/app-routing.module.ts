import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
    AngularFireAuthGuard,
    canActivate,
    redirectLoggedInTo,
    redirectUnauthorizedTo,
} from '@angular/fire/auth-guard';
import { LoginComponent } from './features/login/login.component';

const redirectLoggedInToDashboard = () => redirectLoggedInTo(['/dashboard']);
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/login']);

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
        // canActivate: [AngularFireAuthGuard],
        // data: { authGuardPipe: redirectLoggedInToDashboard },
    },
    {
        path: 'dashboard',
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectUnauthorizedToLogin },
        loadChildren: () => import('./features/dashboard/dashboard.module').then((m) => m.DashboardModule),
    },
    {
        path: 'login',
        canActivate: [AngularFireAuthGuard],
        data: { authGuardPipe: redirectLoggedInToDashboard },
        component: LoginComponent,
    },
    {
        path: '**',
        redirectTo: 'login',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
