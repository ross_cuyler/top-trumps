import { LoginModule } from './features/login/login.module';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { CommonModule } from '@angular/common';
import { NGRXModule } from './core/ngrx/ngrx.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirebaseModule } from './features/shared/firebase.module.ts';
import { LoadingSpinnerModule } from './features/shared/components/loading-spinner/loading-spinner.component';





@NgModule({
    declarations: [AppComponent,   ],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FirebaseModule,
        NGRXModule,
        LoadingSpinnerModule,
        LoginModule,
    ],
    providers: [AngularFireAuthGuard],
    bootstrap: [AppComponent],
})
export class AppModule {}
