import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from './core/ngrx/reducers/index';
import { UploadDetailsService } from './core/services/upload-details.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'skeleton';
    appState$ = this.store.pipe(select(fromRoot.getAppState));

    constructor(private store: Store<fromRoot.State>, uploadservice: UploadDetailsService) {

       // uploadservice.CreateDeckData("Deck1","urltest");
       // uploadservice.UpdateDeckData("Deck2","urltest","QMzMWeggzYUf6lQOKZGB");
      // uploadservice.ReadDeck();
    }
}
