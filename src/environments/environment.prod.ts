export const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyCOHRprhIMOWbAYZdJ48A4Qlr36SJUwltU",
        authDomain: "top-trumps-82dad.firebaseapp.com",
        databaseURL: "https://top-trumps-82dad.firebaseio.com",
        projectId: "top-trumps-82dad",
        storageBucket: "top-trumps-82dad.appspot.com",
        messagingSenderId: "626370380694",
        appId: "1:626370380694:web:7fff204a7bd67689a713b7"
    },
    LOGO: '../../../assets/images/logofont.PNG',
    DASHBOARD_ROUTE: '/dashboard',
};
