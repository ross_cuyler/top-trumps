// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyCOHRprhIMOWbAYZdJ48A4Qlr36SJUwltU",
        authDomain: "top-trumps-82dad.firebaseapp.com",
        databaseURL: "https://top-trumps-82dad.firebaseio.com",
        projectId: "top-trumps-82dad",
        storageBucket: "top-trumps-82dad.appspot.com",
        messagingSenderId: "626370380694",
        appId: "1:626370380694:web:7fff204a7bd67689a713b7"
    },
    LOGO: '../../../assets/images/logofont.PNG',
    DASHBOARD_ROUTE: '/dashboard',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
