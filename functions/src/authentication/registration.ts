import {
  CallableContext,
  HttpsError
} from "firebase-functions/lib/providers/https";
import { firestore, auth } from "../modules/firebase";
// import { cloneDeep } from "lodash";

//#region Helper functions

// async function getGenericUser(email: string) {
//   try {
//     const querySnapshot = await firestore
//       .collection("Users")
//       .where("email", "==", email)
//       .where("archived", "==", false)
//       .get();

//     return {
//       ...querySnapshot.docs[0].data(),
//       uid: querySnapshot.docs[0].id
//     };
//   } catch (error) {
//     throw error;
//   }
// }

async function createGenericUser(email: string, password: string) {
  try {
    return await auth.createUser({ password, email });
  } catch (error) {
    return Promise.reject(error);
  }
}

async function createGenericDatabaseUser(obUser: any) {
  // #region Setting Custom User Claims
  try {
    // TODO:  #Jacques (Where the function has to be called to send email verification.)
    const userRef = firestore.doc(`Users/${obUser.uid}`);
    const doesExist: boolean = (await userRef.get()).exists;
    let prmDatabaseUpdate: Promise<any>;
    if (!doesExist) {
      prmDatabaseUpdate = userRef.set(obUser); // Creates the document for the first time user on the first login.
    } else {
      prmDatabaseUpdate = userRef.update(obUser); // Updates the user if the action is requested.
    }
    await prmDatabaseUpdate;
    return "success";
  } catch (error) {
    console.log("Error: " + error);
    await auth.deleteUser(obUser.uid);
    return Promise.reject(
      "An error occurred while attempting to create the user on the database"
    );
  }
  // #endregion
}

//#endregion

export const registerUser = async (data: any, context: CallableContext) => {
  if (!context.auth) {
    const { obUser, password } = data;
    try {
      const firebaseUser = await createGenericUser(obUser.email, password);
      const obNewUser = {
        ...obUser,
        uid: firebaseUser.uid
      };
      await createGenericDatabaseUser(obNewUser);
      // TODO: Send the verification email to the user.
      return auth.getUser(firebaseUser.uid);
    } catch (error) {
      console.log("error: ", error);

      throw new HttpsError("unimplemented", error.code);
    }
  } else {
    throw new HttpsError("already-exists", "auth/signed-in");
  }
};
