import * as functions from "firebase-functions";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onCall(registerUser);

let registerUser;

if (
  !process.env.FUNCTION_NAME ||
  process.env.FUNCTION_NAME === "registerUser"
) {
  registerUser = functions.https.onCall(
    require("./authentication/registration").registerUser
  );
}

export { registerUser };
